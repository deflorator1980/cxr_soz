# coding: utf-8

import os
import time
import keras
import numpy as np
import argparse

from datetime import datetime
from keras.applications.inception_resnet_v2 import InceptionResNetV2
from keras.layers import Flatten, Dense
from keras.models import Model

started_time = datetime.now()
date_fmt = '%d-%m-%Y_%H-%M-%S'

parser = argparse.ArgumentParser(description='Benchmark parameters')
parser.add_argument('-s', '--n_samples', type=int, default=1000,
                    help='Number of samples to be generated')
parser.add_argument('-c', '--n_classes', type=int, default=1000,
                    help='Number of classes to be generated')
parser.add_argument('-e', '--n_epochs', type=int, default=11,
                    help='Number of training epochs')
parser.add_argument('-m', '--max_batch_size', type=int, default=256,
                    help='Maximum batch size.\
                     The value will be reduced to the nearest one that is a power of 2 (32 - minimum).')
parser.add_argument('-b', '--single_batch_size',  type=int, default=None,
                    help='If specified, then training will be conducted only with one batch_size. '
                         'The value will be reduced to the nearest one that is a power of 2 (32 - minimum).')
parser.add_argument('-a', '--add', required=False,
                    default='',
                    help='addition to results filename')

result_dir = 'results'
os.makedirs(result_dir, exist_ok=True)


def build_model(img_shape=(224, 224, 3), n_classes=1000, load_pretrained=False, freeze_layers_from='base_model'):
    # Decide if load pretrained weights from imagenet
    if load_pretrained:
        weights = 'imagenet'
    else:
        weights = None

    # Get base model
    base_model = InceptionResNetV2(include_top=False, weights=weights,
                       input_tensor=None, input_shape=img_shape)

    # Add final layers
    x = base_model.output
    x = Flatten()(x)
    predictions = Dense(n_classes, activation='softmax', name='fc1000')(x)

    # This is the model we will train
    model = Model(input=base_model.input, output=predictions)

    # Freeze some layers
    if freeze_layers_from is not None:
        if freeze_layers_from == 'base_model':
            print ('Freezing base model layers')
            for layer in base_model.layers:
                layer.trainable = False
        else:
            for i, layer in enumerate(model.layers):
                print(i, layer.name)
            print ('Freezing from layer 0 to ' + str(freeze_layers_from))
            for layer in model.layers[:freeze_layers_from]:
                layer.trainable = False
            for layer in model.layers[freeze_layers_from:]:
                layer.trainable = True
                
    model.compile(loss='categorical_crossentropy',
                  optimizer='Adam',
                  metrics=['accuracy'])

    return model 


class TimeHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.times = []

    def on_epoch_begin(self, batch, logs={}):
        self.epoch_time_start = time.time()

    def on_epoch_end(self, batch, logs={}):
        self.times.append(time.time() - self.epoch_time_start)


def data_generator(n_samples, n_classes):
    input_shape = (n_samples, 224, 224, 3)

    x = np.random.randint(0, 255, input_shape)
    y = np.random.randint(0, n_classes, (input_shape[0],))
    y = keras.utils.to_categorical(y, n_classes)

    x = np.true_divide(x, 255)
    x = x.astype('float32')
    y = y.astype('float32')
    return x, y


def time_stat(time_callback):
    mean = np.mean(time_callback.times[1:])
    total = sum(time_callback.times[1:])
    return total, mean


def generate_batch_sizes(max_value):
    batch_sizes = [4]
    value = 64
    while value <= max_value:
        batch_sizes.append(value)
        value *= 2

    return batch_sizes


def define_batch_size(single_batch_size):
    value = 4
    while True:
        if value * 2 >= single_batch_size:
            return[value]
        value *= 2


def main(args):

    if args.single_batch_size is None:
        batch_sizes = generate_batch_sizes(args.max_batch_size)
    else:
        batch_sizes = define_batch_size(args.single_batch_size)

    X_train, y_train = data_generator( args.n_samples, args.n_classes)

    results = {}
    for batch_size in batch_sizes:
        print('-' * 30)
        print('batch_size:', batch_size)

        model = build_model(n_classes=args.n_classes)
        time_callback = TimeHistory()

        model.fit(X_train, y_train, batch_size=batch_size, epochs=args.n_epochs,
                  shuffle=True, callbacks=[time_callback], verbose=1)

        total_time, mean_time = time_stat(time_callback)
        print('total time: {0:.3f}, time per epoch: {1:.3f}'.format(total_time, mean_time))

        results[batch_size] = (total_time, mean_time)

    filename = os.path.join(result_dir,
                            '{}_benchmark_{}.txt'.format(args.add, started_time.strftime(date_fmt)))

    with open(filename, "w") as text_file:
        print('number of samples: {}'.format(args.n_samples), file=text_file)
        print('number of classes {}'.format(args.n_classes), file=text_file)
        print('number of epochs: {}'.format(args.n_epochs), file=text_file)
        for batch_size, (total, mean) in results.items():
            out = "batch_size = {0}: total time: {1:.3f}, time per epoch: {2:.3f}".format(batch_size, total, mean)
            print(out, file=text_file)


if __name__ == '__main__':
    args = parser.parse_args()
    main(args)

